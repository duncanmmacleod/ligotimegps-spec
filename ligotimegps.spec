%define name    ligotimegps
%define version 2.0.1
%define release 1

Name:      %{name}
Version:   %{version}
Release:   %{release}%{?dist}
Summary:   A pure-python version of lal.LIGOTimeGPS

License:   GPLv3
Url:       https://pypi.org/project/%{name}/
Source0:   https://pypi.io/packages/source/l/%{name}/%{name}-%{version}.tar.gz

Vendor:    Duncan Macleod <duncan.macleod@ligo.org>

BuildArch: noarch
Prefix: %{_prefix}

# needed to parse the spec file (to generate the src.rpm)
BuildRequires: python-srpm-macros

# needed to actually execute the builds
BuildRequires: python2-rpm-macros
BuildRequires: python3-rpm-macros
BuildRequires: python%{python3_pkgversion}
BuildRequires: python%{python3_pkgversion}-setuptools
BuildRequires: python%{python3_pkgversion}-pytest

%description
This module provides a pure-python version of the `LIGOTimeGPS` class,
used to represent GPS times (number of seconds elapsed since GPS epoch)
with nanoseconds precision.

This module is primarily for use as a drop-in replacement for the 'official'
`lal.LIGOTimeGPS` class (provided by the SWIG-python bindings of
LAL <https://wiki.ligo.org/DASWG/LALSuite> for use on those environments
where LAL is not available, or building LAL is unnecessary for the
application (e.g. testing).

The code provided here is much slower than the C-implementation provided by
LAL, so if you really care about performance, don't use this module.

%package -n python%{python3_pkgversion}-%{name}
Summary:  %{summary}
Requires: python%{python3_pkgversion}-six
%{?python_provide:%python_provide python%{python3_pkgversion}-%{name}}
%description -n python%{python3_pkgversion}-%{name}
This module provides a pure-python version of the `LIGOTimeGPS` class, used to represent GPS times (number of seconds elapsed since GPS epoch) with nanoseconds precision.

This module is primarily for use as a drop-in replacement for the 'official' `lal.LIGOTimeGPS` class (provided by the SWIG-python bindings of [LAL](//wiki.ligo.org/DASWG/LALSuite)) for use on those environments where LAL is not available, or building LAL is unnecessary for the application (e.g. testing).

The code provided here is much slower than the C-implementation provided by LAL, so if you really care about performance, don't use this module.

# -- build steps

%prep
%setup -n %{name}-%{version}

%build
%py3_build

%install
%py3_install

%check
%{__python3} -m pytest --pyargs %{name}

%files -n python%{python3_pkgversion}-%{name}
%license LICENSE
%doc README.md
%{python3_sitelib}/*

# -- changelog

%changelog
* Thu Jun 20 2019 Duncan Macleod <duncan.macleod@ligo.org>
- Initial packaging for RHEL
